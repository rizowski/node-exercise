const express = require('express');

const { ValidationError } = require('express-validation');

const logger = require('./lib/logger');
const routes = require('./routes');
const clients = require('./lib/clients');
const controllers = require('./lib/controllers');

const app = express();

const port = process.env.PORT || 4000;
const host = process.env.HOST || '0.0.0.0';

app.use((_, res, next) => {
  res.locals = {
    controllers: controllers.create({
      clients,
      logger,
    }),
    logger,
  };

  next();
});

app.use((req, _, next) => {
  logger.info({
    remoteAddress: req.remoteAddress,
    path: req.path,
    method: req.method,
  });

  next();
});

routes.register({ app });

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  if (err instanceof ValidationError) {
    return res.status(err.statusCode).json(err);
  }

  return res.status(500).json(err);
});

app.listen(port, host, () => {
  logger.info(`http://${host}:${port}`);
});
