// const { Router } = require("express");
const logger = require('../lib/logger');

const people = require('./people');
const planets = require('./planets');

const allRoutes = [people, planets];

const mapRoutes =
  (app) =>
  ([, r]) => {
    const handler = app[r.method].bind(app);

    r.validation
      ? handler(r.uri, r.validation, r.handler)
      : handler(r.uri, r.handler);

    logger.debug(`${r.method}:${r.uri} loaded`);
  };

exports.register = ({ app }) => {
  allRoutes.forEach((group) => {
    // const router = new Router();

    Object.entries(group).forEach(mapRoutes(app));

    // app.use(group.baseRoute, router);
  });
};
