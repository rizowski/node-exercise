const { validate, Joi: joi } = require('express-validation');

const baseRoute = '/people';

exports.getAll = {
  method: 'get',
  uri: baseRoute,
  handler: async ({ query }, res) => {
    const {
      locals: { controllers },
    } = res;

    const results = await controllers.people.getAll(query);

    res.json(results);
  },
  validation: validate({
    query: joi.object({
      sortBy: joi.string().valid('height', 'mass', 'name'),
    }),
  }),
};
