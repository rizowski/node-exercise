const baseRoute = '/planets';

exports.getAll = {
  method: 'get',
  uri: baseRoute,
  handler: async (req, res) => {
    const {
      locals: { controllers },
    } = res;

    const results = await controllers.planets.getAll();

    res.json(results);
  },
};
