const pino = require('pino');

const { STAGE, LOG_LEVEL = 'info' } = process.env;

function createConfig() {
  const baseConfig = {
    name: process.env.HOST || 'local',
    level: LOG_LEVEL,
    redact: ['headers.authorization'],
  };

  if (['local', 'test'].includes(STAGE)) {
    return {
      ...baseConfig,
      level: 'trace',
      prettyPrint: {
        ignore: 'pid,hostname',
      },
    };
  }

  if (STAGE === 'test') {
    return {
      ...baseConfig,
      level: 'silent',
    };
  }

  return baseConfig;
}

const logger = pino(createConfig());

module.exports = logger;
