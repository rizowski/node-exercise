exports.format = (person) => {
  const { height, mass, ...rest } = person;

  return {
    ...rest,
    height: Number.parseInt(height, 10),
    mass: Number.parseInt(mass, 10),
  };
};

exports.formatMany = (people) => people?.map(exports.format);

const sort = (items, sortByTerm) => {
  if (!sortByTerm) {
    return items;
  }

  return items.sort((a, b) => {
    const aItem = a[sortByTerm];
    const bItem = b[sortByTerm];

    if (aItem < bItem) {
      return -1;
    }

    if (aItem > bItem) {
      return 1;
    }

    return 0;
  });
};

exports.create = ({ clients, logger }) => {
  const { swapi, recursiveGet, baseClient } = clients;

  return {
    async getAll({ sortBy }) {
      const results = await recursiveGet(await swapi.get('/people'));

      logger.debug({ total: results.length }, 'Retrieved People');

      return sort(exports.formatMany(results), sortBy);
    },
    async fetchManyByUrl(urls) {
      return Promise.all(
        urls.map(async (url) => {
          const { data } = await baseClient.get(url);
          return data;
        })
      );
    },
  };
};
