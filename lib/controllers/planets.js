const { create: creatPeopleCtrl } = require('./people');

exports.format = (planet) => planet;
exports.formatMany = (planets) => planets?.map(exports.format);

exports.create = ({ clients, logger }) => {
  const { swapi, recursiveGet } = clients;

  const peopleCtrl = creatPeopleCtrl({ clients, logger });

  return {
    async getAll() {
      const planets = await recursiveGet(await swapi.get('/planets'));

      const detailedPlanets = await Promise.all(
        planets.map(async (p) => {
          if (!p.residents) {
            return p;
          }

          const people = await peopleCtrl.fetchManyByUrl(p.residents);

          const { residents, ...rest } = p;

          return {
            ...rest,
            residents: people.map((p) => p.name),
          };
        })
      );

      return detailedPlanets;
    },
  };
};
