const people = require('./people');
const planets = require('./planets');

exports.create = (config) => ({
  people: people.create(config),
  planets: planets.create(config),
});
