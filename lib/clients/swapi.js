const axios = require('axios').default;

module.exports = axios.create({
  baseURL: 'https://swapi.dev/api',
});
