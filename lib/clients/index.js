const baseClient = require('axios').default;
const swapi = require('./swapi');
const logger = require('../logger');

const recursiveGet = async (promise, allResults = []) => {
  try {
    const { data } = await promise;

    if (!data.next) {
      return allResults.concat(data.results);
    }

    return await recursiveGet(
      baseClient.get(data.next),
      allResults.concat(data.results)
    );
  } catch (error) {
    logger.error(error);
  }
};

module.exports = { swapi, recursiveGet, baseClient };
