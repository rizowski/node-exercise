const ctrls = require('../lib/controllers');

const createClients = () => ({
  swapi: {
    get: jest.fn().mockRejectedValue(new Error('mock swapi.get')),
  },
  baseClient: {
    get: jest.fn().mockRejectedValue(new Error('mock baseClient.get')),
  },
  recursiveGet: jest
    .fn(async (promise) => promise)
    .mockRejectedValue(new Error('mock recursiveGet')),
});

exports.resetMocks = () => {
  jest.resetAllMocks();

  const clients = createClients();
  const logger = {
    debug: () => {},
    info: () => {},
  };
  const controllers = ctrls.create({ clients, logger });

  const res = {
    json: jest.fn(),
    locals: {
      clients,
      controllers,
      logger,
    },
  };

  return {
    clients,
    controllers,
    res,
  };
};
