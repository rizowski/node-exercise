const people = require('../../routes/people');
const stubs = require('../stubs');
const data = require('../data/people.json');

describe('routes/people', () => {
  let clientStub;
  let resStub;

  beforeEach(() => {
    ({ res: resStub, clients: clientStub } = stubs.resetMocks());
  });

  it('grabs all the people', async () => {
    clientStub.swapi.get.mockResolvedValueOnce(data);
    clientStub.recursiveGet
      .mockResolvedValueOnce(data.results)
      .mockResolvedValueOnce({ results: [] });
    await people.getAll.handler({ query: {} }, resStub);

    expect(resStub.json).toHaveBeenCalledTimes(1);
    expect(resStub.json.mock.calls[0][0]).toMatchSnapshot();
  });

  it('sorts people by name', async () => {
    clientStub.swapi.get.mockResolvedValueOnce(data);
    clientStub.recursiveGet
      .mockResolvedValueOnce(data.results)
      .mockResolvedValueOnce([]);

    await people.getAll.handler({ query: { sortBy: 'name' } }, resStub);

    expect(resStub.json).toHaveBeenCalledTimes(1);
    expect(resStub.json.mock.calls[0][0].map(({ name }) => ({ name }))).toEqual(
      [
        { name: 'Beru Whitesun lars' },
        { name: 'Biggs Darklighter' },
        { name: 'C-3PO' },
        { name: 'Darth Vader' },
        { name: 'Leia Organa' },
        { name: 'Luke Skywalker' },
        { name: 'Obi-Wan Kenobi' },
        { name: 'Owen Lars' },
        { name: 'R2-D2' },
        { name: 'R5-D4' },
      ]
    );
  });

  it('sorts people by height', async () => {
    clientStub.swapi.get.mockResolvedValueOnce(data);
    clientStub.recursiveGet
      .mockResolvedValueOnce(data.results)
      .mockResolvedValueOnce([]);

    await people.getAll.handler({ query: { sortBy: 'height' } }, resStub);

    expect(resStub.json).toHaveBeenCalledTimes(1);
    expect(
      resStub.json.mock.calls[0][0].map(({ name, height }) => ({
        name,
        height,
      }))
    ).toEqual([
      { name: 'R2-D2', height: 96 },
      { name: 'R5-D4', height: 97 },
      { name: 'Leia Organa', height: 150 },
      { name: 'Beru Whitesun lars', height: 165 },
      { name: 'C-3PO', height: 167 },
      { name: 'Luke Skywalker', height: 172 },
      { name: 'Owen Lars', height: 178 },
      { name: 'Obi-Wan Kenobi', height: 182 },
      { name: 'Biggs Darklighter', height: 183 },
      { name: 'Darth Vader', height: 202 },
    ]);
  });

  it('sorts people by mass', async () => {
    clientStub.swapi.get.mockResolvedValueOnce(data);
    clientStub.recursiveGet
      .mockResolvedValueOnce(data.results)
      .mockResolvedValueOnce([]);

    await people.getAll.handler({ query: { sortBy: 'mass' } }, resStub);

    expect(resStub.json).toHaveBeenCalledTimes(1);
    expect(
      resStub.json.mock.calls[0][0].map(({ name, mass }) => ({ name, mass }))
    ).toEqual([
      { name: 'R2-D2', mass: 32 },
      { name: 'R5-D4', mass: 32 },
      { name: 'Leia Organa', mass: 49 },
      { name: 'C-3PO', mass: 75 },
      { name: 'Beru Whitesun lars', mass: 75 },
      { name: 'Luke Skywalker', mass: 77 },
      { name: 'Obi-Wan Kenobi', mass: 77 },
      { name: 'Biggs Darklighter', mass: 84 },
      { name: 'Owen Lars', mass: 120 },
      { name: 'Darth Vader', mass: 136 },
    ]);
  });
});
