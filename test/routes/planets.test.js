const planets = require('../../routes/planets');
const stubs = require('../stubs');
const data = require('../data/planets.json');

describe('routes/planets', () => {
  let clientStub;
  let resStub;

  beforeAll(() => {
    ({ res: resStub, clients: clientStub } = stubs.resetMocks());
  });

  it('grabs planets and residents', async () => {
    clientStub.swapi.get.mockResolvedValue(data);
    clientStub.recursiveGet.mockResolvedValueOnce(data.results);
    clientStub.baseClient.get.mockResolvedValue({ data: { name: 'Yoda' } });

    await planets.getAll.handler({}, resStub);

    expect(resStub.json).toHaveBeenCalledTimes(1);
    expect(resStub.json.mock.calls[0][0]).toMatchSnapshot();
  });
});
